<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.kun</groupId>
    <artifactId>kun-picture-viewer</artifactId>
    <version>1.0</version>
    <name>kun-picture-viewer</name>
    <description>坤坤图片查看器</description>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <java.version>17</java.version>
        <!-- 全局参数 -->
        <runtime-dir-name>runtime</runtime-dir-name>
        <main-class-package-path>com.kun.Main</main-class-package-path>
        <organizationName>坤坤信息科技</organizationName>
        <!-- 插件版本 -->
        <maven-compiler-plugin.version>3.8.1</maven-compiler-plugin.version>
        <javafx-maven-plugin.version>0.0.8</javafx-maven-plugin.version>
        <javapackager-plugin.version>1.7.5</javapackager-plugin.version>
        <!-- javafx版本 -->
        <javafx.version>17.0.1</javafx.version>
        <!-- 第三方库版本 -->
        <junit.version>5.8.1</junit.version>
        <slf4j-api.version>2.0.13</slf4j-api.version>
        <log4j.version>2.23.1</log4j.version>
        <lombok.version>1.18.28</lombok.version>
        <jsoup.version>1.17.2</jsoup.version>
        <image4j.version>0.7.2</image4j.version>
        <bootstrapfx-core.version>0.4.0</bootstrapfx-core.version>
        <controlsfx.version>11.1.0</controlsfx.version>
        <hutool.version>5.8.26</hutool.version>
        <webp.version>0.1.6</webp.version>
        <jackson.version>2.16.2</jackson.version>
        <commons-imaging.version>1.0.0-alpha5</commons-imaging.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-controls</artifactId>
            <version>${javafx.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-fxml</artifactId>
            <version>${javafx.version}</version>
        </dependency>
        <dependency>
            <groupId>org.openjfx</groupId>
            <artifactId>javafx-web</artifactId>
            <version>${javafx.version}</version>
        </dependency>
        <dependency>
            <groupId>org.controlsfx</groupId>
            <artifactId>controlsfx</artifactId>
            <version>${controlsfx.version}</version>
        </dependency>
        <dependency>
            <groupId>org.kordamp.bootstrapfx</groupId>
            <artifactId>bootstrapfx-core</artifactId>
            <version>${bootstrapfx-core.version}</version>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
        <!--用于与slf4j保持桥接（里面自动依赖了slf4j-api）-->
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>${log4j.version}</version>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j-api.version}</version>
        </dependency>
        <!-- 支持图片格式 WebP -->
        <dependency>
            <groupId>org.sejda.imageio</groupId>
            <artifactId>webp-imageio</artifactId>
            <version>${webp.version}</version>
        </dependency>
        <!-- hutool工具类 -->
        <dependency>
            <groupId>cn.hutool</groupId>
            <artifactId>hutool-all</artifactId>
            <version>${hutool.version}</version>
        </dependency>
        <!-- json序列化框架 -->
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
            <version>${jackson.version}</version>
        </dependency>
        <!-- 图片转换icon -->
        <dependency>
            <groupId>net.ifok.image</groupId>
            <artifactId>image4j</artifactId>
            <version>${image4j.version}</version>
        </dependency>
        <!-- 网页爬取 -->
        <dependency>
            <groupId>org.jsoup</groupId>
            <artifactId>jsoup</artifactId>
            <version>${jsoup.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-imaging -->
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-imaging</artifactId>
            <version>${commons-imaging.version}</version>
        </dependency>
    </dependencies>

    <build>
        <finalName>${project.name}</finalName>
        <plugins>
            <!-- jar包打包插件配置 -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven-compiler-plugin.version}</version>
                <configuration>
                    <outputFileName>${project.name}-${project.version}</outputFileName>
                    <source>${java.version}</source>
                    <target>${java.version}</target>
                </configuration>
            </plugin>
            <!-- javafx打包插件配置 -->
            <plugin>
                <groupId>org.openjfx</groupId>
                <artifactId>javafx-maven-plugin</artifactId>
                <version>${javafx-maven-plugin.version}</version>
                <executions>
                    <execution>
                        <id>default-cli</id>
                        <configuration>
                            <mainClass>${main-class-package-path}</mainClass>
                            <launcher>${runtime-dir-name}</launcher>
                            <jlinkZipName>${runtime-dir-name}</jlinkZipName>
                            <jlinkImageName>${runtime-dir-name}</jlinkImageName>
                            <noManPages>true</noManPages>
                            <stripDebug>true</stripDebug>
                            <noHeaderFiles>true</noHeaderFiles>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <!-- exe程序打包插件配置 -->
            <plugin>
                <groupId>io.github.fvarrui</groupId>
                <artifactId>javapackager</artifactId>
                <version>${javapackager-plugin.version}</version>
                <executions>
                    <execution>
                        <id>bundling-for-windows</id>
                        <phase>package</phase>
                        <goals>
                            <goal>package</goal>
                        </goals>
                        <configuration>
                            <!-- 启动类 -->
                            <mainClass>${main-class-package-path}</mainClass>
                            <!-- 绑定自定义JRE路径-->
                            <bundleJre>true</bundleJre>
                            <jreMinVersion>11</jreMinVersion>
                            <jrePath>.\target\${runtime-dir-name}</jrePath>
                            <generateInstaller>false</generateInstaller>
                            <administratorRequired>false</administratorRequired>
                            <!-- 操作系统-->
                            <platform>windows</platform>
                            <copyDependencies>true</copyDependencies>
                            <!-- 名称与版本-->
                            <displayName>kpv</displayName>
                            <name>kpv</name>
                            <version>${project.version}</version>
                            <!-- 手动引入额外资源-->
                            <!--                            <additionalResources>-->
                            <!--                                <additionalResource>D:\Item\GD_AmtHardwareTest1.0\datas</additionalResource>-->
                            <!--                                <additionalResource>D:\Item\GD_AmtHardwareTest1.0\lib</additionalResource>-->
                            <!--                            </additionalResources>-->
                            <!--详细参数配置-->
                            <winConfig>
                                <icoFile>.\src\resources\favicon.ico</icoFile>
                                <generateSetup>false</generateSetup>
                                <generateMsi>false</generateMsi>
                                <generateMsm>false</generateMsm>
                                <headerType>console</headerType>
                            </winConfig>
                            <vmArgs>
                                -Dserver.port=8888
                            </vmArgs>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>