module com.kun {
    requires javafx.controls;
    requires javafx.web;
    requires javafx.fxml;

    requires org.controlsfx.controls;
    requires org.kordamp.bootstrapfx.core;
    requires java.desktop;
    requires com.luciad.imageio.webp;
    requires cn.hutool;
    requires java.sql;
    requires java.base;
    requires org.slf4j;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires org.jsoup;
    requires org.apache.commons.imaging;

    exports com.kun;
    opens com.kun to javafx.fxml;
    exports com.kun.controller;
    opens com.kun.controller to javafx.fxml;
}