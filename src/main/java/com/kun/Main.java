package com.kun;

/**
 * 程序入口
 *
 * @author gzc
 * @since 2024/7/30
 */
public class Main {
    public static void main(String[] args) {
        App.main(args);
    }
}
