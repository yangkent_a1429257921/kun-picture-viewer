package com.kun;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.logging.Logger;

/**
 * 启动类
 *
 * @author gzc
 * @since 2024/7/30
 */
public class App extends Application {
    private final static Logger log = Logger.getLogger(App.class.getName());

    @Override
    public void start(Stage stage) throws IOException {
        log.info("开始构建主窗口...");
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("hello-view.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 400, 300);
        stage.setTitle("Hello!");
        stage.setScene(scene);
        stage.setAlwaysOnTop(true);
        stage.show();
        log.info("已完成主窗口的构建!");
        log.info("程序已启动...");
    }

    public static void main(String[] args) {
        launch();
    }
}