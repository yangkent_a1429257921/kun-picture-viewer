package com.kun.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

/**
 * 测试类
 *
 * @author gzc
 * @since 2024/7/30
 */
public class HelloController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}